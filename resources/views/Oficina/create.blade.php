@extends('Template.main')

@section('table-index')
    <form action="{{ route('oficina.store') }}" method="POST">
        @csrf
        <div class="row">
            <div class="form-control">
                <input type="text" name="name" placeholder="Nombre">
            </div>
            <div class="form-control">
                <input type="text" name="address" placeholder="Dirección">
            </div>
            <div class="form-control">
                <input type="text" name="phone" placeholder="Teléfono">
            </div>
        </div>
        <dov class="row">
            <button type="submit" value="Enviar" class="btn btn-success"/>
        </dov>
    </form>

    <a href="{{ route("oficina.index") }}">Regresar</a>
@endsection